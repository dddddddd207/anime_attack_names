## to use:
`git clone https://gitlab.com/dddddddd207/anime_attack_names.git`

or click the download button, click `.zip`, extract. you'll need (python)[https://www.python.org/downloads/release/python-3112/] to run it. scroll down and click "Windows installer (64-bit)" then run the installer

next open a command prompt or, better, a powershell window by opening the extracted folder, holding shift, right-clicking, and clicking "open a powershell window here"

then type `python ./Script.py` and press enter

open folder, run as always

## to get latest changes
open powershell window in folder, run:

`git pull`

and voila

## to add changes
after you've made and saved your changes, open a powershell window in this folder and run:

`git pull`

`git add .`

`git commit -m 'what you changed'`

`git push`

that way i get the changes too
