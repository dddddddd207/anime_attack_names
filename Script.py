from random import randint
from random import random

import os
import ast

# for finding how many functions in this file so the final generator works
with open(os.path.realpath(__file__)) as file:
    tree = ast.parse(file.read())
    defList = [exp for exp in tree.body if isinstance(exp, ast.FunctionDef) and not exp.name == "main"]
    defSum = sum(isinstance(exp, ast.FunctionDef) for exp in tree.body) - 1

# open the adjectives and nounes files
# if you want to add adverbs or whatever then change this block
with open("adjectives.txt") as adj, open("nouns.txt") as nn:
    adjectives = adj.readlines()
    nouns = nn.readlines()

# to use:
# 1. copy and paste the following couple of lines and remove the # in front
#
# def ():
#    s =
#    return s
#
# 2. in front of the () put the name,
#    preferably using j for adjective and n for noun
#    thus jjn means adjective adjective noun
# 3. copy the below lines, paste above s =, and remove the leading #
#    for multiple adjectives increment the number
#
#    adj0 = adjectives[randint(0, len(adjectives) - 1)].rstrip()
#    nn0 = nouns[randint(0, len(nouns) - 1)].rstrip()
#
# 4. after the s = put [name of variable] + " " + [name of next variable] + " " + etc
#    nothice that it's " " with a space, not just ""
#    if you want to have like a colon or whatever enclose it in quotes and remember to add a space
#
# ----NOTE THIS FILE USES FOUR SPACES FOR INDENTATION----
#     if you open it in notepad using the tab key won't work
#     if you open it in vs code tab will automatically be converted to the correct number of spaces
#

# adjective adjcetive noun
def jjn():
    adj0 = adjectives[randint(0, len(adjectives) - 1)].rstrip()
    adj1 = adjectives[randint(0, len(adjectives) - 1)].rstrip()
    nn0 = nouns[randint(0, len(nouns) - 1)].rstrip()
    s = adj0 + " " + adj1 + " " + nn0
    return s

# adjective noun: adjective noun
def an_colon_an():
    adj0 = adjectives[randint(0, len(adjectives) - 1)].rstrip()
    nn0 = nouns[randint(0, len(nouns) - 1)].rstrip()
    adj1 = adjectives[randint(0, len(adjectives) - 1)].rstrip()
    nn1 = nouns[randint(0, len(nouns) - 1)].rstrip()
    s = adj0 + " " + nn0 + ": " + adj1 + " " + nn1
    return s

# noun of adjective
def n_of_a():
    adj0 = adjectives[randint(0, len(adjectives) - 1)].rstrip()
    nn0 = nouns[randint(0, len(nouns) - 1)].rstrip()
    adj1 = adjectives[randint(0, len(adjectives) - 1)].rstrip()
    nn1 = nouns[randint(0, len(nouns) - 1)].rstrip()
    s = nn0 + " of " + adj0 + ": " + adj1 + " " + nn1
    return s

# noun of adjective: adjective noun
def n_of_a_colon_an():
    adj0 = adjectives[randint(0, len(adjectives) - 1)].rstrip()
    nn0 = nouns[randint(0, len(nouns) - 1)].rstrip()
    s = nn0 + " of " + adj0
    return s

# don't mess with anything below here
def main():
    print(eval(defList[randint(0, defSum - 1)].name + "()"))

if __name__=="__main__":
    main()
